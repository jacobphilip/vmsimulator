package main

import (
	"log"
	"math"
	"math/rand"
	"net/http"
	"runtime"
	"sync"
	"time"
)

const singleUnitSec = 3
const measurementPeriod = 30
var ForceMaxEnabled = false
var isChecked = true

const port = "8090"

type loaderType func()

func init() {
	rand.Seed(time.Now().UnixNano())
}


func main(){
	http.HandleFunc("/force", forceMax)
	http.HandleFunc("/disable", disableForceMax)
	go http.ListenAndServe(":" + port, nil)
	simulateCPULoad()
}

func simulateCPULoad() {
	curMin := 0
	for {
		rMax := randomMax(curMin) + curMin
		rStep := randomStep(rMax - curMin)
		rMin := randomMin(rMax)
		if ForceMaxEnabled == true {
			log.Printf("Force max is enabled. Setting to max")
			rMax = totalUnitsPerMeasurement()
			rStep = 1
			rMin =  rMax - 1
			curMin = rMax - 1
			isChecked = true
		}
		//log.Printf("Generating with curMin = %v rMax = %v, rStep = %v rMin = %v\n", curMin, rMax, rStep, rMin)
		generateSineWaveSingle(curMin, rMax, rStep, rMin)
		curMin = rMin
		if curMin < 2 {
			time.Sleep(measurementPeriod * time.Second)
		}
	}
}

func randomMax(curMin int) int{
	max := rand.Intn(totalUnitsPerMeasurement() - curMin)
	if max < 2 {
		max = 2
	}
	return max
}

func randomStep(max int) int{
	step := rand.Intn(max/2)
	if step < 2 {
		step = 1
	}
	return step
}

func randomMin(max int) int {
	min := rand.Intn(max-1)
	if min < 1 {
		min = 0
	}
	return min
}
func generateSineWaveSingle(current int, max int, step int, min int){
	log.Printf("Generating with curMin = %v rMax = %v, rStep = %v rMin = %v\n", current, max, step, min)
	for i := current+1; i <= max; i += step {
		loadCPU(i)
		idleCPU(totalUnitsPerMeasurement() - i)
	}
	for i := max-step; i > min; i -= step {
		loadCPU(i)
		idleCPU(totalUnitsPerMeasurement() - i)
	}
}

func totalUnitsPerMeasurement() int{
	return measurementPeriod / (singleUnitSec * 4)
}
func idleCPU(count int)  {
	//log.Printf("sleeping for %v secs", count * singleUnitSec)
	for ; count > 0; count-- {
		time.Sleep(singleUnitSec * time.Second)
		if ForceMaxEnabled == true && isChecked == false {
			isChecked = true
			return
		}
	}
}

func loadCPU(count int)  {
	//log.Printf("loading for %v secs", count * singleUnitSec)
	for ; count > 0; count-- {
		loadCPUSingleUnit()
		if ForceMaxEnabled == true && isChecked == false {
			isChecked = true
			return
		}
	}
}

func loadCPUSingleUnit(){
	wg := &sync.WaitGroup{}
	noOfCpus := runtime.NumCPU()
	for ; noOfCpus > 0; noOfCpus-- {
		wg.Add(1)
		go doLoadCPU(wg, singleUnitSec)
	}
	wg.Wait()
}

func doLoadCPU(wg *sync.WaitGroup, loopSeconds float64) {
	defer wg.Done()
	start := time.Now()
	elapsed := start.Sub(start)
	for elapsed.Seconds() < loopSeconds{
		math.Sqrt(1399345.9999)
		elapsed = time.Now().Sub(start)
	}
}

func forceMax(w http.ResponseWriter, req *http.Request) {
	log.Println("Enabling Force max")
	ForceMaxEnabled = true
	isChecked = false
}
func disableForceMax(w http.ResponseWriter, req *http.Request)  {
	log.Println("Disabling Force max")
	ForceMaxEnabled = false
}